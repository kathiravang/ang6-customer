import { Component, OnInit, Input, OnChanges, SimpleChanges } from "@angular/core";
import { Customer } from "../customer";

@Component({
  selector: "app-grid",
  templateUrl: "./grid.component.html",
  styleUrls: ["./grid.component.css"]
})
export class GridComponent implements OnInit, OnChanges {

  @Input()
  headers: string[];

  @Input()
  data: any[];

  tableData: string[][] = [];

  


  // tdata = [["1","Vsn"],["2","Vsn"]]
  /* data = [{

	    "id" : 1,
      "name": "Jeevan",
      "location": "Chennai",
"type":"Retail"
  },{

"id" : 2,
      "name": "Jim",
      "location": "Ashburn",
"type":"Retail"
  }, {

"id" : 3,
      "name": "Putin",
      "location": "Moscow",
"type":"Enterprise"
  }

]*/

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    if(this.headers && this.data) {
      console.log(true);
      let dt = [{ "id" : 1, "name": "Jeevan",
              "location": "Chennai",
        "type":"Retail"
          },{
  
        "id" : 2,
              "name": "Jim",
              "location": "Ashburn",
        "type":"Retail"
          }, {
  
        "id" : 3,
              "name": "Putin",
              "location": "Moscow",
        "type":"Enterprise"
          }
  
      ];
      console.log(dt);
      let row = [];
      for(let td of dt) {
        let column = [];
        for(let hi of this.headers) {
          console.log(hi);
          column.push(td[hi]);
        }
        row.push(column);
  
      }
      console.log(row);

      this.tableData = row;

    }
    


  }

  ngOnInit() {
    console.log(this.headers);
    console.log(this.data);
  }
}
