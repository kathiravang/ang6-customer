export interface Customer {
  id: number;
  name: string;
  location: string;
  type: string;
}
