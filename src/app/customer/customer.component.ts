import { Component, OnInit } from "@angular/core";
import { CustomerAPIService } from "../customer-api.service";
import { Customer } from "../customer";

@Component({
  selector: "app-customer",
  templateUrl: "./customer.component.html",
  styleUrls: ["./customer.component.css"]
})
export class CustomerComponent implements OnInit {
  customers: Customer[];
  headers: string[];

  constructor(private customerService: CustomerAPIService) {}

  ngOnInit() {
    this.customerService
      .getCustomers()
      .subscribe(data => (this.customers = data));

    this.headers = ['name', 'type'];
  }
}
