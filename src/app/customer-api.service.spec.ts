import { TestBed } from '@angular/core/testing';

import { CustomerAPIService } from './customer-api.service';

describe('CustomerAPIService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomerAPIService = TestBed.get(CustomerAPIService);
    expect(service).toBeTruthy();
  });
});
